const sacMedecin = 'http://localhost:3001/assets/photoPage/Atelier-cuir-des-fuchats-sac-medecin-marron-clair.jpeg'
const sacDos = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats_sac_a_dos_classique.jpeg'
const porteMonnaie = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-3-porte-monnaie.jpeg'
const sacCeintureBleue = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-sac_ceinture_bleu.jpeg'
const sacAbidjanMarron = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-sac-abidjan-marron-motifs.jpeg'
const sacAbidjanRose = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-sac-abidjan-rose.jpeg'
const sacCabasMarron = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-sac-cabas-2marrons.jpeg'
const sacCabasBeige = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-sac-cabas-beige.jpeg'
const sacMichele = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-Sac-Michele-marron.jpeg'
const sacOrange = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-Sac-rond-orange.jpeg'
const sacRougeMetal = 'http://localhost:3001/assets/photoPage/atelier-cuir-fuchats-sac-rouge-armatures-metal.jpeg'
const bracelets = 'http://localhost:3001/assets/photoPage/bracelets.jpeg'
const etuiPhoto = 'http://localhost:3001/assets/photoPage/Etui-appareil-photo-cuir-artisanal-profil.jpeg'
const sacMedecinVoyage = 'http://localhost:3001/assets/photoPage/sac-voyage-medecin-cuir.jpeg'
const sacocheMarronClaire = 'http://localhost:3001/assets/photoPage/sacoche-homme-cuir-marron-clair-artisanat.jpeg'
const CartableMarronClaire = 'http://localhost:3001/assets/cartableMarronClair/cartable-cuir-marron-clair.jpeg'
const sacCarreMarron = 'http://localhost:3001/assets/sacCarreMarron/sac-cuir-artisanal-rectangle-marron.jpeg'
const sacocheMarron = 'http://localhost:3001/assets/sacocheMarron/sacoche-homme-cuir-marron-artisanat-fuchats.jpeg'
const sacPascale = 'http://localhost:3001/assets/sacPascale/Sac-cuir-artisanal-marron-pascale-devant.jpeg'
const sacRouge = 'http://localhost:3001/assets/sacRouge/Petit-sac-rouge-rond-veau-dessus.jpeg'



const photos = [
    {
        id: 1,
        cover : sacMichele,
        name: 'sac Michèle'
    },
    {
        id: 2,
        cover : sacOrange,
        name : 'sac Orange'
    },
    {
        id: 3,
        cover : sacCabasBeige,
        name: 'sac cabas beige'
    },
    {
        id: 4,
        cover : sacCabasMarron,
        name : 'sac cabas marron'
    },
    {
        id: 5,
        cover : sacDos,
        name : 'sac à dos'
    },
    {
        id: 6,
        cover : sacMedecin,
        name : 'sac médecin'
    },
    {
        id: 7,
        cover : sacMedecinVoyage,
        name : 'sac medecin de voyage'
    },
    {
        id: 8,
        cover : sacCeintureBleue,
        name : 'sac ceinture bleue'
    },
    {
        id: 9,
        cover : sacAbidjanMarron,
        name: 'sac abidjan marron'
    },
    {
        id: 10,
        cover : sacAbidjanRose,
        name : 'sac Abidjan Rose'
    },
    {
        id: 11,
        cover : sacRougeMetal,
        name : 'sac rouge armature metale'
    },
    {
        id: 12,
        cover : sacocheMarronClaire,
        name : 'sacoche marron claire'
    },
    {
        id: 13,
        cover : sacCarreMarron,
        name : 'sac carre marron'
    },
    {
        id: 14,
        cover : sacPascale,
        name : 'sac sacPascale'
    },
    {
        id: 15,
        cover : sacRouge,
        name : 'sac rouge'
    },
    {
        id: 16,
        cover : sacocheMarron,
        name : 'sac marron'
    },
    {
        id: 17,
        cover : CartableMarronClaire,
        name : 'cartable marron claire'
    },
    {
        id: 18,
        cover : bracelets,
        name : 'bracelets'
    },
    {
        id: 19,
        cover : porteMonnaie,
        name : 'porte monnaie'
    },
    {
        id: 20,
        cover : etuiPhoto,
        name : 'etui photo'
    },
];

module.exports = photos;