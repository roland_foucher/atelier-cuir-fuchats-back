const blanot = 'http://localhost:3001/assets/news/affiche-blanot-annule.jpeg'
const michele = 'http://localhost:3001/assets/workshop/Michele-Foucher-Atelier-cuir-fuchats.jpeg'

const news = [
    {
        title: 'Marchés de Noël 2021',
        alert : 'annulé !',
        comment : 'L\'Atelier Cuir des Fuchats sera présent au marché de Noël de Blanot le 5 décembre 2021 ainsi qu\'au marché de Noël de Solutré à la Maison du Grand Site les 18, 19 et 20 décembre 2021.',
        cover : blanot,
        id : 1,
    },
    {
        title: 'L\'atelier cuir',
        alert : '',
        comment : 'L\'Atelier Cuir des Fuchats vous accueil dans son atelier sur randez-vous. N\'hésitez pas à nous contacter pour venir voir nos articles!',
        cover : michele,
        id : 2,
    }
];

module.exports = news;